// build sufiix array take O(n*(log n) * (log n ) )



//include <paBezanAshghal>
#include <bits/stdc++.h>
#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(ll J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<  long long ,pii >
#define 	pdd         		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        100000+99
#define 	EPS 		        1e-10
#define 	inf 		        1ll<<61
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define 	vpii         		vector<pii>
#define 	vll        		    vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);
typedef long long ll;
typedef long double ld;
using namespace std;
inline ll get_num(char ch){
	if(ch == '-')return 0;
	else if(ch >= 'a' && ch <= 'z'){
		return 1 + (ch-'a');
	}
	else if(ch >= 'A' && ch <= 'Z'){
		return 27 +(ch - 'A');
	}
	else if(ch >= '0' && ch <= '9'){
		return 53 +(ch - '0');
	}
}
//inline ll pw(ll x ,ll y){
//    if(y==0) return 1;
//    ll p = pw(x ,y/2);
//    p*=p;
//    if(y&1) p*=x;
//    return p;
//}
 int dx[] = {1,0,-1, 0} , dy[] = {0,1, 0, -1};  // 4 Direction
//int dx[] = {1,0,-1, 0 , 2 ,0 ,-2, 0 , 3 ,0 ,-3 ,0} ,
 //   dy[] = {0,1, 0, -1 , 0 ,2 ,0 , -2 , 0 , 3, 0 , -3};  // 4 Direction

 //int dx[] = {1,-1,0,0,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1,1,-1}; // 8 Direction
/* int dx[] = {1,-1,1,-1,2,2,-2,-2} , dy[] = {2,2,-2,-2,1,-1,1,-1}; */ // Knight Direction
/* int dx[] = {2,-2,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1}; */ // Hexagonal Direction
inline int GA(ll n , vector<ll>&vec){
    Rep(i ,n){ll x ;cin>>x ; vec.push_back(x);}
    return 0;
}
inline int GAEG(ll m ,vector<ll>vec[]){
    Rep(i ,m){ll x  , y ;cin>>x>>y ; x-- , y-- ; vec[x].push_back(y); vec[y].push_back(x);}
    return 0;
}

ll pw(ll x, ll y){
    if(y==0) return 1;
    ll o = y/2;
    ll f = pw(x, o);
    if(y%2){
        return f*f*x;
    }
    else return f*f;
}

const ll MD = 1e9+7;
using namespace std;


struct suffix{
    int ind;
    int rnk[2];
} sfxarray[500000];


bool cmp(suffix &a , suffix &b){
    return (a.rnk[0]==b.rnk[0])? (a.rnk[1]<b.rnk[1] ? true : false) : (a.rnk[0]<b.rnk[0] ? true: false );
}

ll ind[500000];
ll suffixarrray[500000];
ll BS(string s){
    Rep(i , s.size()){
        sfxarray[i].ind = i;
        sfxarray[i].rnk[0] = s[i]-'a';
        sfxarray[i].rnk[1] = ((i+1)<s.size()) ? s[i+1]-'a' : -1;
    }
    sort(sfxarray , sfxarray+s.size() , cmp);
    ll cur = 0;
    for(int k  = 4;  k<2*(s.size()) ; k*=2){
        cur = 0;
        ll pre = sfxarray[0].rnk[0];
        sfxarray[0].rnk[0] = cur;
        For(i  , 1,  s.size()){
            if(sfxarray[i].rnk[0]== pre){
                pre = sfxarray[i].rnk[0];
                if(sfxarray[i].rnk[1]==sfxarray[i-1].rnk[1]){
                        sfxarray[i].rnk[0] = cur;
                }
                else{
                                  sfxarray[i].rnk[0] = ++cur;
                }
            }
            else{
                pre = sfxarray[i].rnk[0];
                sfxarray[i].rnk[0] = ++cur;
            }
            ind[sfxarray[i].ind] = i;
        }
        Rep(i , s.size()){
            ll nxt_rnk =sfxarray[i].ind+ (k/2);
            if(nxt_rnk>=s.size()){ sfxarray[i].rnk[1] =-1 ; continue; }
            int idx = ind[nxt_rnk];
            sfxarray[i].rnk[1] = sfxarray[idx].rnk[0];
        }
        sort(sfxarray , sfxarray+s.size() , cmp);
    }
    Rep(i , s.size()) suffixarrray[i]= sfxarray[i].ind;
    return 0;
}
int main(){
    string s;
    cin>>s;
    BS(s);
    Rep(i , s.size()) cout<<suffixarrray[i]<<" ";
    return 0;
}





