//include <paBezanAshghal>
#include <bits/stdc++.h>
#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(ll J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<  pii ,long long  >
#define 	pdd         		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        100000+99
#define 	EPS 		        1e-10
#define 	inf 		        1ll<<61
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define 	vpii         		vector<pii>
#define 	vll        		    vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);
typedef long long ll;
typedef long double ld;
using namespace std;
inline ll get_num(char ch){
	if(ch == '-')return 0;
	else if(ch >= 'a' && ch <= 'z'){
		return 1 + (ch-'a');
	}
	else if(ch >= 'A' && ch <= 'Z'){
		return 27 +(ch - 'A');
	}
	else if(ch >= '0' && ch <= '9'){
		return 53 +(ch - '0');
	}
}
//inline ll pw(ll x ,ll y){
//    if(y==0) return 1;
//    ll p = pw(x ,y/2);
//    p*=p;
//    if(y&1) p*=x;
//    return p;
//}
 int dx[] = {1,0,-1, 0} , dy[] = {0,1, 0, -1};  // 4 Direction
//int dx[] = {1,0,-1, 0 , 2 ,0 ,-2, 0 , 3 ,0 ,-3 ,0} ,
 //   dy[] = {0,1, 0, -1 , 0 ,2 ,0 , -2 , 0 , 3, 0 , -3};  // 4 Direction

 //int dx[] = {1,-1,0,0,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1,1,-1}; // 8 Direction
/* int dx[] = {1,-1,1,-1,2,2,-2,-2} , dy[] = {2,2,-2,-2,1,-1,1,-1}; */ // Knight Direction
/* int dx[] = {2,-2,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1}; */ // Hexagonal Direction
inline int GA(ll n , vector<ll>&vec){
    Rep(i ,n){ll x ;cin>>x ; vec.push_back(x);}
    return 0;
}
inline int GAEG(ll m ,vector<ll>vec[]){
    Rep(i ,m){ll x  , y ;cin>>x>>y ; x-- , y-- ; vec[x].push_back(y); vec[y].push_back(x);}
    return 0;
}

ll pw(ll x, ll y){
    if(y==0) return 1;
    ll o = y/2;
    ll f = pw(x, o);
    if(y%2){
        return f*f*x;
    }
    else return f*f;
}

const ll MD = 1e9+7;
using namespace std;


string s = "";
ll lcp[6000000];
ll su [6000000];
ll idx[6000000];
struct ST{
    ll a, b;
    ll ind;
} v[6000000];
ll arr[10];
ST ot[6000000];

ll CNTSORTA(ll q , ll sz){
    Set(arr, 0);
    Rep(i , sz ){
        ll o  = ((v[i].a)/q)%10;
        arr[o]++;
    }
    For(i ,1, 10) arr[i]+=arr[i-1];
    for(int i = sz-1;  i>=0;i--){
        ll o  = ((v[i].a)/q)%10;
        if(arr[o]){
            arr[o]--;
            ot[arr[o]] = v[i];
        }
    }
    Rep(i , sz){
        v[i] = ot[i];
    }
    return 0;
}
ll CNTSORTB(ll q , ll sz){
    Set(arr, 0);
    Rep(i , sz ){
        ll o  = ((v[i].b+1)/q)%10;
        arr[o]++;
    }
    For(i ,1, 10) arr[i]+=arr[i-1];
    for(int i = sz-1;  i>=0;i--){
        ll o  = ((v[i].b+1)/q)%10;
        if(arr[o]){
            arr[o]--;
            ot[arr[o]] = v[i];
        }
    }
    Rep(i , sz){
        v[i] = ot[i];
    }
    return 0;
}
ll RXSORT(ll n){
    ll mx  = 0;
    Rep(i , n){
        mx= max(mx,  v[i].b+1);
    }
    ll p = 1;
    while(true){
        ll r = mx/p;
        if(!r) break;
        CNTSORTB(p , n);
        p*=10;
    }
    mx  = 0;
    Rep(i , n){
        mx= max(mx,  v[i].a);
    }
    p = 1;
    while(true){
        ll r = mx/p;
        if(!r) break;
        CNTSORTA(p , n);
        p*=10;
    }
    return 0;
}
ll FF(){
    Rep(i , s.size()){
        ll x = s[i]-'a';
        ll y;
        if(i+1 < s.size())
         y = s[i+1]-'a';
         else y =-1;
         v[i].a = x;
         v[i].b = y;
         v[i].ind = i;
    }
    RXSORT(s.size());
    ll n = s.size();
    ll p  = 2*n+1;
    For(k ,4 , p){
        ll rnk = 0;
        ll pre  = v[0].a;
        v[0].a = rnk;
        idx[v[0].ind] = 0;

        For(i , 1, s.size()){
            if(v[i].a == pre){
                if(v[i].b==v[i-1].b){
                    v[i].a=rnk;
                }
                else v[i].a = ++rnk;
            }
            else{
                pre = v[i].a;
                v[i].a = ++rnk;
            }
            idx[v[i].ind] = i;
        }
        Rep(i , s.size()){
            ll f = v[i].ind + (k/2);
            if(f>=s.size()){
                v[i].b = -1;
                continue;
            }
            ll c = idx[f];
            v[i].b = v[c].a;
        }
        RXSORT(s.size());

    }
    Rep(i , n){
        su[i] = v[i].ind;
    }
    return 0;
}

ll LC(){
    ll c = 0;
    Rep(i  ,s.size()){
        idx[su[i]]  = i;
    }
    Rep(i , s.size()){
        ll o = idx[i];
        if(o==s.size()-1){
            c= 0;
            continue;
        }
        ll p = su[idx[i]+1];
        while( i+c<s.size() && p+c<s.size() && s[p+c] == s[c+i] ) c++;
        lcp[idx[i]] = c;
        if(c) c--;
    }
    return 0;
}
int main(){
//    Test;
  booste;
    string a , b;
    cin>>a>>b;
    s= a+"$" + b;
    FF();
    LC();
    ll mx = 0;
    Rep(i , s.size()-1){
        if((su[i] < a.size() && su[i+1] > a.size()) || (su[i+1] < a.size() && su[i] > a.size())){
                mx= max(mx,  lcp[i]);
        }
    }
    cout<<mx<<endl;
    return 0 ;
}
